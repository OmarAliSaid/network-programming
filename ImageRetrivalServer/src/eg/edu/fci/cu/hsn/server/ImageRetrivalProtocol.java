/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.fci.cu.hsn.server;

import static com.sun.xml.internal.ws.model.RuntimeModeler.PORT;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author midO nO mOre
 */
public class ImageRetrivalProtocol {
    private static Socket socket;
    public void connect()
    {
        try
        {
 
            int port = 1222;
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server Started and listening to the port 1222");
 
            //Server is running always. This is done using this while(true) loop
            while(true)
            {
                //Reading the message from the client
                socket = serverSocket.accept();
                ClientThread client = new ClientThread(socket);
                client.run();                
               
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                socket.close();
            }
            catch(Exception e){}
        }
    
    }
    
    public String[] listImages()
    {
        String[] image_names = new String[100];
        File folder = new File("C:\\Users\\midO nO mOre\\Documents\\NetBeansProjects\\ImageRetrivalServer\\imageRepository");
        File[] listOfFiles = folder.listFiles();

       for (int i = 0; i < listOfFiles.length; i++)
        {
          if (listOfFiles[i].isFile()) 
          {
           image_names[i]=listOfFiles[i].getName();
          }
        }
        return image_names;
        
        
    }
    
    public String line_string(String[] image_names)
    {
        String line="";

         for(int i=0;i<image_names.length;i++)
            {
             if(image_names[i]!=null)
             line+= image_names[i]+"  ";
            }
        return line;
    }
    
    public byte[] processInput(String imageName) throws FileNotFoundException, IOException
    {
        FileInputStream fis = new FileInputStream("C:\\Users\\midO nO mOre\\Documents\\NetBeansProjects\\ImageRetrivalServer\\imageRepository\\"+imageName);
        byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        return buffer;
        
    }
    
}
