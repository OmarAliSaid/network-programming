/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.fci.cu.hsn.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author midO nO mOre
 */
public class ClientThread extends Thread {
    protected Socket socket;
    ImageRetrivalProtocol obj;
    
    public ClientThread(Socket clientSocket) throws IOException {
        obj = new ImageRetrivalProtocol();
        socket = clientSocket;
    }

         public void run() {
                InputStream is = null;
        try {
            is = socket.getInputStream();
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String command = null;
        try {
            command = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
                System.out.println("Message received from client is "+command);
 
                //check command
                String returnMessage="";
                if(command.equals("list"))
                {
                    returnMessage=obj.line_string(obj.listImages())+"\n";
                }
                else if(command.contains("jpg"))
                {
                    byte[]img = null;
                    String encoded_img = "";
                    
                    try {
                        img= obj.processInput(command);
                    } catch (IOException ex) {
                        Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    if(img!=null)
                    {
                        for(int i=0;i<img.length;i++)
                        {
                           encoded_img+=img[i]+" ";
                        }
                        
                        returnMessage=encoded_img+"\n";
                    }
                    else
                    {
                        returnMessage="Error In File name "+"\n";
                    }
                    
                }
                
               
 
                //Sending the response back to the client.
                OutputStream os = null;
        try {
            os = socket.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
        try {
            bw.write(returnMessage);
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
                System.out.println("Message sent to the client is "+returnMessage);
        try {
            bw.flush();
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}