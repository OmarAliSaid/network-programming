/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eg.edu.fci.cu.hsn.client;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.imageio.ImageIO;

/**
 *
 * @author midO nO mOre
 */
public class ImageRetrivalClient {

  
    private static Socket socket;
    
   public static String command="",message="";
   
    public static void main(String[] args) throws IOException {
        Scanner Input = new Scanner(System.in);
        
        System.out.println("Enter list To list images names");
        command = Input.nextLine();
        connect();
        System.out.println("list is \n"+message);
        
        System.out.println("Enter Image name to download");
        command = Input.nextLine();
        connect();
        
 
        System.out.println(command);
        if(message.contains("Error"))
        {
            System.out.println("Error In Image Name");
        }
        else
        {
           String[] splited = message.split("\\s+");
           
           byte[]img=new byte[splited.length];
           
            
           for(int i=0;i<splited.length;i++)
           {    
                 img[i]= Byte.valueOf(splited[i]);      
           }
           
            write_image(img);
            
            System.out.println("Done");
        }
    }
    
    private static void connect()
    {
        
        try
        {
            String host = "127.0.0.1";
            int port = 1222;
            InetAddress address = InetAddress.getByName(host);
            socket = new Socket(address, port);
 
            //Send the message to the server
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
 
           
 
            String sendMessage = command + "\n";
            bw.write(sendMessage);
            bw.flush();
            System.out.println("Message sent to the server : "+sendMessage);
 
            //Get the return message from the server
            InputStream is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
             message = br.readLine();
            
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            //Closing the socket
            try
            {
                socket.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
    }
    
    private static void write_image(byte[] image) throws IOException
    {
        OutputStream out = new FileOutputStream("C:\\Users\\midO nO mOre\\Documents\\NetBeansProjects\\ImageRetrivalClient\\downloads\\"+command);
        out.write(image);   
        out.close();
    }
    
    
}
